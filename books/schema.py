import graphene
from graphene_django import DjangoObjectType
from .models import Books

class BooksType(DjangoObjectType):
    class Meta:
        model = Books
        fields = ("id","title","excerpt")
        """
        Above creates a graph
        Each model should be converted like above 
        type Books{
            id:id
            title: String
            excerpt: String
        }
        """
class Query(graphene.ObjectType):
    # creates graph
    all_books = graphene.List(BooksType)

    def resolve_all_books(root,info):
        # we use filter here
        return Books.objects.all()

schema = graphene.Schema(query=Query)
